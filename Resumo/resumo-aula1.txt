AULA 1 ********************************************************************************************************

CRIAR 
    ->CONTA NO GITLAB
    ->CONTA NA AWS
    ->DOCKER HUB

INSTALAR 
    ->GIT
    ->VSCODE

CRIAR REPOSITORIO NO GIT LAB
    ->DEIXAR PUBLICO
    ->E LOCALMENTE FAZER ISSO
        CRIAR UMA PASTA LOCAL PARA O PROJETO
            DEVOPS-WEEK
        git config --global user.name "odirleisias"
        git config --global user.email "odirleisias@gmail.com"
        
CLONAR O REPOSITORIO PARA MAQUINA LOCAL
        git clone https://gitlab.com/odirleisias/devops-aws.git

ADIONAR O CONTEUDO DA AULA 1 NO REPOSITORIO LOCAL (DEVOPS-WEEK)

SUBIR CONTEUDO NO REPOSITORIO DO GIT LAB
    IR EM CHANGES >> CLICAR NO MAIS>> DAR UM NOME PARA O COMMIT>> 
    CLICAR NO "V" ACIMA>> IR NOS 3 PONTINHOS E CLICAR EM PUSH

INSTALAÇÃO DO JENKINS EM UMA INSTANCIA DA AMAZON USANDO O userdata-jenkins.sh
            sudo yum update -y
            sudo amazon-linux-extras install epel -y
            sudo yum install daemonize -y
            sudo wget -O /etc/yum.repos.d/jenkins.repo \
                https://pkg.jenkins.io/redhat-stable/jenkins.repo
            sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
            sudo yum upgrade
            sudo yum install jenkins java-1.8.0-openjdk-devel -y
            sudo systemctl daemon-reload
            sudo systemctl start jenkins
            sudo cat /var/lib/jenkins/secrets/initialAdminPassword
            sudo chmod 666 /var/run/docker.sock

            *****************************************************
            sudo yum update -y                           # Atualiza os softwares disponiveis para o sistema
            sudo yum install git -y                      # Instala o git
            sudo amazon-linux-extras install docker -y   # Instala o docker
            sudo systemctl start docker                  # Inicia o docker
            sudo usermod -aG docker jenkins              # Da permissões ao Jenkins para rodar comandos Docker
            sudo systemctl restart jenkins               # Reinicia o Jenkins para aplicar as permissões anteriormente adicionadas
            *****************************************************


    ->SUBIU UMA INSTANCIA EC2 E COLOCOU OS CÓDIGOS PARA INSTALAR ASSIM QUE SUBIU A INSTANCIA
    ->COPIOU A CHAVE DO JENKINS
        sudo cat /var/lib/jenkins/secrets/initialAdminPassword
437d6ea7b6a24281a6fc53460c4806df
    ->SUBIU O JENKINS NA PORTA 8080 
    ->A TAG NAME - JENKINS
    -> COLOCAR PARA SELECIONAR OS PLUGINS A SER INSTALADOS
        -> OS PLUGINS SELECIONADOS SÃO
            -> GITLAB E O GIT PARAMETER
                -> INSTALL
    -> PRONTO PARA USO


    AULA 2 ********************************************************************************************************

    CRIAR UMA INSTANCIA EC2 COM DADOS DA AULA 2
        -> ESTA INSTANCIA IRA CONTER O DOCKER
    NOME DA INSTANCIA EC2 É 
        ->APP-SERVER
            #!/bin/bash
            sudo apt-get update
            sudo apt-get upgrade -y
            sudo apt-get install docker.io git -y
            sudo usermod -aG docker ubuntu
            sudo chmod 666 /var/run/docker.sock

    
    BAIXA UM CONTAINER NGNX RODANDO NA PORTA 80
        -> docker run -itd -p 80:80 nginx
    
    ABRIR O GIT LAB E BAIXAR O REPOSITORIO 
        -> git clone https://gitlab.com/odirleisias/devops-aws.git
    
    IR NA PASTA DA APLICAÇÃO EM NODE E RODAR O DOCKER BUILD (O PARAMETRO -t SERVE PARA DAR NOME PARA IMAGEM)
        -> A APLICAÇÃO ENCONTRA-SE NA PASTA AULA-1/APP
        ->docker build -t O7-CLOUD . 
        ->ANTES PRECISA EXCLUIR O CONTAINER DO NGNX
        ->docker run -itd -p 80:3000 o7-cloud

    HISTORY DA APLICAÇÃO
     1  docker --version
    2  docker run -itd -p 80:80 nginx
    3  docker ps
    4  history
    5  LS
    6  ls
    7  git clone https://gitlab.com/odirleisias/devops-aws.git
    8  ls
    9  cd devops-aws/
   10  ls
   11  cd aula-
   12  cd aula-1/
   13  ls
   14  cd app/
   15  ls
   16  docker build -t o7-cloud .
   17  docker images
   18  docker ps
   19  docker rm -f c24fc02bd5d6
   20  docker ps
   21  docker run -itd -p 80:3000 o7-cloud
   22  history


    CRIAR CONTA NO DOCKER HUB
        -> ESTE ARMAZENA AS IMAGENS DOCKER
        
    AULA 3 ********************************************************************************************************
        -> NESTA AULA IRA SUBIR UM CONTAINER NA MÃO COM UMA APLICAÇÃO NODE

        FAZER LOGIN DENTRO DO TERMINAL
        COMANDO 
        docker login
            ->usuario: odirlei
            ->senha: @rgonio#docker
        
        CRIAR UM REPOSITORIO NO DOCKER HUB
            ->https://hub.docker.com/repositories

            docker push odirlei/o7-cloud:tagname
            -> O COMANDO FICOU ASSIM    
                ->docker tag o7-cloud:latest odirlei/o7-cloud (PARA RENOMEAR A IMAGEM CRIADA)
                ->docker push odirlei/o7-cloud (PARA SUBIR ELA NO REPOSITORIO)
                ->docker run -itd -p 80:3000 odirlei/o7-cloud
            PROCESSO DE CRIAÇÃO E POSTAGEM DA IMAGEM NO REPOSITORIO FEITO COM SUCESSO

            https://hub.docker.com/repository/docker/odirlei/o7-cloud

        -> jenkins 00:46

        ALGUMAS CONFIGURAÇOES BASICAS
            -> IR EM NOVO JOB >> COLOCAR O NOME DO JOB >> FREE STYLE
            -> COLOCAR UMA DESCRIÇÃO E MARCAR A CHECK BOX "Este build é parametrizado"
                ->ADIONAR PARAMETRO - ESCOLHA - COLOCAR O NOME DA VARIÁVEL E AS ESCOLHAS (VALORES)
                    ->NOME: AMBIENTE
                    ->ESCOLHAS
                        ->PRODUCAO
                        ->HOMOLOGACAO
                        ->TESTE
                -> PODE SE COLOCAR MAIS PARAMETROS FAZENDO O MESMO PASSO ARTERIOR
            -> BEM NA PARTE INFERIOR, IR EM 
                ->BUILD >> EXECUTAR SHELL
                    ->COLOCAR UM SHELL SCRIPT QUE IRA EXECUTAR O JOB       
            ->SALVAR 

            -> IR EM CONTRUIR COM PARAMETROS 
                -> E ESCOLHER OS PARAMETROS QUE FORAM SETADOS 
                ->CLICAR EM CONSTRUIR
                -> IR EM HISTORICO DE BUILDS
                    ->E NO LADO ESQUERDO IR EM SAIDA DO CONSOLE
                        ->MOSTRARÁ A SAIDA QUE FOI EXECUTADO 

                -> PRECISE EDITAR ALGUMA CONFIGURAÇAO, NA TELA INICIAL DO JENKINS IR NO JOB > CONFIGURAR
                -> NA MESMA TELA PODE SE EXCLUIR O JOB

        
        -> PIPELINE.GROOVY 1:00:00 ********************************************************
            SERÁ USADO O SCRIP DA AULA 3 PIPELINE.GROOVY
            -> PRIMEIRO É NECESSARIO CRIAR A CREDENCIAL DE ACESSO AO DOCKER
                ->IR EM GERENCIAR JENKINS > MANAGE CREDENTIALS > JENKINS > GLOBAL CREDENTIALS
                    -> NO CANTO ESQUERDO "ADD CREDENTIALS"
                        -> KIND - USERNAME WITH PASSWORD
                        -> POE O NOME DO USUARIO "odirlei"
                        -> COLOCA A SENHA
                        -> POE O ID (NO EXEMPLO) dockerhub_id
        
        -> CRIAR UM NOVO JOB
            -> COLOCAR NOME E IR EM PIPELINE - OK
                -> EM PIPELINE (BEM EM BAIXO)
                    -> COLAR O SCRIP GROOVY COM AS DEVIDAS MUDANÇAS (AULA 3)
            
            -> DEPOIS DO JOB CRIADO - EXECUTAR ELE E IR EM CONTRUIR AGORA 
                ->ACOMPANHAR OS LOG PARA VER SE ESTA TUDO CERTO

        COMANDO PARA ACOMPANHAR LOG NO DOCKER  (1:33)
            -> docker logs -f "id-container"
                
                1:41 aula 3
    -> CRIAR UMA ROLE PARA O CODEBUILD
        -> IR EM USUARIOS IAM
            ->IR EM FUNCOES
            ->CRIAR FUNCAO - IR EM BAIXO E LOCALIZAR POR CODE DEPLOY
                ->SELECIONAR CODE DEPLOY > SELECIONAR CASO DE USO COMO CODE-DEPLOY
                ->AVANÇAR - ATE NOME DA FUNCAO - "CODEDEPLOY-ROLE" > CRIAR FUNCAO
        ->NECESSARIO CRIAR MAIS UMA FUNÇAO - ESSA É UM CASO DE USO COMO EC2 > AVANÇAR 
            -> ESCOLHER AMAZONS3FULLACESS > NOME COMO "S3FULLACESS" - CRIAR FUNÇAO
            -> 


    
    

